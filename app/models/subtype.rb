class Subtype < ApplicationRecord
    belongs_to :type
    has_many :tariffs
end
