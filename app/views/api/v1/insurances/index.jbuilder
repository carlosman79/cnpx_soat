json = Jbuilder.new do |j|
	j.message 'ok'
	j.status true
	j.data @insurance do |is|
		j.merge! is.as_json
		j.state @insurance.where(['until >= ?',Time.now]).present?
		j.taker is.taker.as_json
		j.vehicle is.vehicle.as_json
	end
end
