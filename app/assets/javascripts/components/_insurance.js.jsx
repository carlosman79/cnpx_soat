var Insurance = React.createClass({
    getInitialState() {
        return { editable: false }
    },
    handleEdit() {
        if(this.state.editable) {
           
        }
        this.props.handleUpdate(this.state);
        this.setState({ editable: !this.state.editable })
    },


    render() {
        return (
            <tr key={this.props.insurance.id}>
                <td>{this.props.insurance.taker.names} {this.props.insurance.taker.lastnames}</td>
                <td>{this.props.insurance.vehicle.plate}</td>
                <td>{this.props.insurance.cost}</td>
                <td>{this.props.insurance.fosyga}</td>
                <td>{this.props.insurance.runt}</td>
                <td>{this.props.insurance.bought_at}</td>
                <td>{this.props.insurance.until}</td>
                <td>{this.props.insurance.medics}</td>
                <td>{this.props.insurance.death}</td>
                <td>{this.props.insurance.inability}</td>
                <td><button onClick={this.handleEdit}> {this.state.editable ? 'Submit' : 'Edit' } </button></td>
            </tr>
        )
    }
});