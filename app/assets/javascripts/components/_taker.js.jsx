var Taker = React.createClass({
    getInitialState() {
        return {editable: false}
    },
    handleEdit() {
        if(this.state.editable) {
           
        }
        this.props.handleUpdate(this.state);
        this.setState({ editable: !this.state.editable })
    },


    render() {
        return (
            <tr key={this.props.taker.id}>
                <td> 
                    <select className="form-control" onChange={ (e) => this.setState({ doctype: e.target.value }) } ref='doctype' placeholder='Entre tipo de documento'>
                        <option value="CC">Cédula de ciudadanía</option>
                        <option value="CE">Cédula de extranjería</option>
                        <option value="PS">Pasaporte</option>
                    </select> 
                </td>
                <td><input defaultValue={this.props.taker.docnumber} className="form-control" type="text" onChange={ (e) => this.setState({ docnumber: e.target.value }) } ref='docnumber' placeholder='Entre número de documento' /> </td>
                <td><input defaultValue={this.props.taker.names}  className="form-control" onChange={ (e) => this.setState({ names: e.target.value }) } ref='names' placeholder='Nombres' /> </td>
                <td><input defaultValue={this.props.taker.lastnames} className="form-control" onChange={ (e) => this.setState({ lastnames: e.target.value }) } ref='lastnames' placeholder='Apellidos' /></td>
                <td><input defaultValue={this.props.taker.email}  className="form-control" type="email" onChange={ (e) => this.setState({ email: e.target.value }) } ref='email' placeholder='Email' /></td>
                <td><input defaultValue={this.props.taker.phone}  className="form-control" type="phone" onChange={ (e) => this.setState({ phone: e.target.value }) } ref='phone' placeholder='Telefono' /></td>
                <td><button onClick={this.props.handleDelete} >Delete</button></td>
                <td><button onClick={this.handleEdit}> {this.state.editable ? 'Submit' : 'Edit' } </button></td>
            </tr>
        )
    }
});