var _timeout = null;

var NewInsurance = React.createClass({ 
	getInitialState() {
		var cils = <input className="form-control" onChange={ (e) => this.setState({ tariff_params: {cils: e.target.value, type_id: this.state.type_id} }) } ref="cils" type="number" placeholder="Cilindraje" />
		var passengers = <input className="form-control" onChange={ (e) => this.setState({ tariff_params: {passengers: e.target.value, type_id: this.state.type_id} }) } ref="passengers" type="number" placeholder="Pasajeros" />
		var weight = <input className="form-control" onChange={ (e) => this.setState({tariff_params: { weight: e.target.value, type_id: this.state.type_id }}) } ref="weight" type="number" placeholder="Peso" />
		var age = <input className="form-control" onChange={ (e) => this.setState({ tariff_params: {age: e.target.value, type_id: this.state.type_id }}) } ref="age" type="number" placeholder="Edad" />

		var subtype1 = ['',cils, cils, weight, cils,cils,cils,cils, '', passengers]
		var subtype2 = ['','', age, '', '',age,age,age, '', '']

		return {step: 0,tariff:{}, tariff_params: {}, takers: [], vehicles: [], vehicle: null, insurances: [], new: false, types: [],subtypes: [],type_id: 0, subtype1: subtype1, subtype2: subtype2}
	},
	componentDidMount () {
		 $.getJSON('/api/v1/takers.json', (response) => { this.setState({ takers: response }) });
		 $.getJSON('/api/v1/vehicles.json', (response) => { this.setState({ vehicles: response }) });
		 $.getJSON('/api/v1/vehicles/get_types.json', (response) => { this.setState({ types: response }) });
	},
	componentDidUpdate () {
		console.log('componentDidUpdate')
		console.log(this.state.tariff_params)
	},
	searchInsurances (plate)  {
		var that = this
		clearTimeout(_timeout)
		_timeout = setTimeout(function(){
			console.log('calling_insurances')
			$.getJSON('/api/v1/insurances/get_insurances.json/',{ plate: plate }, (response) => { that.setState({insurances: response.data, plate: plate}) });
		},500);
	},
	searchSubtypes (type_id)  {
		console.log(type_id)
		var that = this
		clearTimeout(_timeout)
		_timeout = setTimeout(function(){
			console.log('calling_sybtypes')
			$.getJSON('/api/v1/vehicles/get_subtypes.json/',{ id: type_id }, (response) => { that.setState({type_id: type_id, subtypes: response}) });
		},500);

	},
	newInsurance () {
		var that = this
		this.setState({new: true, step: this.state.step +1 })
		switch (this.state.step){
			case 1:
					$.getJSON('/api/v1/tariffs/get_tariff.json/',{ tariff_params: that.state.tariff_params }, (response) => { that.setState({tariff: response}) });
			break;
			case 2:

			break;
			case 3:
					that.handleClick()
			break;
		}

	},
	handleClick() { 
		console.log('handleClick')
		var that = this
		$.ajax({ 	url: '/api/v1/insurances', 
					type: 'POST', 
					data: { 
						insurance: that.state.tariff,
						vehicle: {plate: that.state.plate,type_id: that.state.type_id, subtype_id: that.state.tariff.subtype_id},
						taker: that.state.taker
					}, success: (insurance) => { 
						this.setState({step: 0, new: false})
						console.log(insurance)
					} 
				});
	},
	handleTaker(taker) {
		console.log(taker)
		this.setState({taker: taker, step: this.state.step+1 })
		this.handleClick()
	},
	render() {
		takers = []
		vehicles = []
		types = []
		subtypes = []
		vehicles.push(<option key={-1} value={-1}>{'Seleccione'}</option>)

		this.state.vehicles.forEach((vehicle) => {
			vehicles.push(<option key={vehicle.id} value={vehicle.id}>{vehicle.plate}</option>)
		})		
		takers.push(<option key={-1} value={-1}>{'Seleccione'}</option>)
		this.state.takers.forEach((taker) => {
			takers.push(<option key={taker.id} value={taker.id}>{taker.names} {taker.lastnames}</option>)
		})
		types.push(<option key={-1} value={-1}>{'Seleccione'}</option>)
		this.state.types.forEach((type) => {
			types.push(<option key={type.id} value={type.id}>{type.name}</option>)
		})
		subtypes.push(<option key={-1} value={-1}>{'Seleccione'}</option>)
		this.state.subtypes.forEach((subtype) => {
			subtypes.push(<option key={subtype.id} value={subtype.id}>{subtype.name}</option>)
		})
		console.log(this.state.type_id)
	 return ( 
		 <div className="container">

			<div className="form-group"> 
				{!this.state.new ?
					<input className="form-control"  onChange={ (e) => this.searchInsurances(e.target.value) } ref='plate' placeholder='Vehiculo' /> :
					<h1>Nueva poliza SOAT para {this.state.plate}</h1>
				}


				{this.state.new ?
					<div className="form-group"> 
						
						<select className="form-control" onChange={ (e) => this.searchSubtypes(e.target.value) } ref='type_id' placeholder='Tipo'>
							{types}
						</select> 
						{this.state.subtype1[this.state.type_id]}
						{this.state.subtype2[this.state.type_id]}
						{this.state.step==2 ? 
						<div className="panel panel-default">
							<div className="panel-heading">
  								<h2>Orden de compra</h2>
							</div>
							<div className="panel-body">
								<div className="form-group">
									<label className="label label-default">Tasa Comercial</label>
									<span className="pull-right">{this.state.tariff.rate}</span>
								</div>
								<div className="form-group"><label className="label label-default">Valor Prima</label><span className="pull-right">{this.state.tariff.cost}</span></div>
								<div className="form-group"><label className="label label-default">Contribución Fosyga</label><span className="pull-right">{this.state.tariff.fosyga}</span></div>
								<div className="form-group"><label className="label label-default">Subtotal Prima y Fosyga</label><span className="pull-right">{this.state.tariff.subtotal}</span></div>
								<div className="form-group"><label className="label label-default">Tasa RUNT</label><span className="pull-right">{this.state.tariff.runt}</span></div>
							</div>
							<div className="panel-footer">
								<div className="form-group">
									<h3 className="">Total a pagar                              {this.state.tariff.total}</h3>
								</div>
							</div>
							<div className="panel-heading">
								<h4>Cobertura</h4>
								<div className="panel-body">
									<h4>Muertes y gastos funerarios {this.state.tariff.death} SMLDV</h4>
									<h4>Gastos médicos {this.state.tariff.medics} SMLDV</h4>
									<h4>Incapacidad permanente {this.state.tariff.inability} SMLDV</h4>
									<h4>Movilización {this.state.tariff.move} SMLDV</h4>
								</div>
							</div>
						</div> : ''}
						{this.state.step==3 ? 
						<div className="panel panel-default">
							<div className="panel-heading">
  								<h2>Datos de pago</h2>
							</div>
							<div className="panel-body">
								<NewTaker handleSubmit={this.handleTaker} />
							</div>
						</div> : ''}
					</div>
				: ''}
			</div>
			{this.state.insurances.length>0 && !this.state.new  ? 
			<div className="form-group">
				<h1>Vehiculo ya asegurado!</h1>
				<AllInsurances insurances={this.state.insurances} /> 
			</div> : <span>{!this.state.new ? <h1>Vehiculo no tiene Soat vigentes</h1> : ''}</span> }
				
			<div className="">
				<button className="btn btn-primary"  onClick={this.newInsurance} >Asegurar</button> 
			</div> 
	
		</div>
		) 
	} 
});
