var Body = React.createClass({
    getInitialState() {
        return { takers: [], insurances: [] }
    },


    componentDidMount() {
        $.getJSON('/api/v1/takers.json', (response) => { this.setState({ takers: response }) });
        $.getJSON('/api/v1/insurances.json', (response) => { this.setState({ insurances: response.data }) });
        console.log(this.props.option)
    },



    handleSubmit(taker) {
        var newState = this.state.takers.concat(taker);
        this.setState({ takers: newState })
    },

    handleSubmitInsurance(insurance) {
        var newState = this.state.takers.concat(insurance);
        this.props.option = 2
        this.setState({ insurances: newState })
    },
    handleDelete(id) {
        $.ajax({
            url: `/api/v1/takers/${id}`,
            type: 'DELETE',
            success:() => {
               this.removeTakerClient(id);
            }
        });
    },

    removeTakerClient(id) {
        var newTakers = this.state.takers.filter((taker) => {
            return taker.id != id;
        });

        this.setState({ takers: newTakers });
    },



    handleUpdate(taker) {
        $.ajax({
                url: `/api/v1/takers/${taker.id}`,
                type: 'PUT',
                data: { takers: taker },
                success: () => {
                    this.updateTakers(taker);

                }
            }
        )},

    updateTakers(taker) {
        var takers = this.state.takers.filter((i) => { return i.id != taker.id });
        takers.push(taker);

        this.setState({takers: takers });
    },


    render() {
    	console.log('body render')
        consultar = []
        //consultar.push(<NewTaker key={1} handleSubmit={this.handleSubmit}/>)
        //consultar.push(<AllTakers key={2} takers={this.state.takers}  handleDelete={this.handleDelete} onUpdate={this.handleUpdate}/>)
        consultar.push(<AllInsurances key={2} insurances={this.state.insurances}  handleDelete={this.handleDelete} onUpdate={this.handleUpdate}/>)
        comprar = []
        comprar.push(<NewInsurance handleSubmitInsurance={this.handleSubmitInsurance} key={1} />)
        rendered = []
        if (this.props.option==1)
            rendered = comprar
        if (this.props.option==2)
            rendered = consultar

        return (
            <div className='container'>
                {rendered}
            </div>
        )
    }
});