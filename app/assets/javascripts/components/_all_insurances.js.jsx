var AllInsurances = React.createClass({ 
	getInitialState() { 
		return {
		 insurances: []
		} 
	},
	handleDelete(id) {
        this.props.handleDelete(id);
    },
    onUpdate(taker) {
        this.props.onUpdate(taker);
    },
	componentDidMount() { 
		console.log('Component mounted'); 

	}, 
	render() { 
		var thead = ( <thead  key={"thead"+999999}> 
					    <tr key={"tr"+999999}>
							<th key={"th"+1}>Tomador</th> 
							<th key={"th"+2}>Vehiculo</th> 
							<th key={"th"+3}>Costo</th> 
							<th key={"th"+4}>Fosyga</th> 
							<th key={"th"+5}>Runt</th>
							<th key={"th"+6}>Comprado en</th> 
							<th key={"th"+7}>Vence en</th> 
							<th key={"th"+8}>Cobertura Médica</th> 
							<th key={"th"+9}>Cobertura Muerte</th> 
							<th key={"th"+10}>Cobertura Incapacidad</th>
						</tr>
					</thead> )
		var insurances = this.props.insurances.map((insurance) => { 
			return (
				<Insurance key={"in" + insurance.id} handleUpdate={this.onUpdate} handleDelete={this.handleDelete.bind(this, insurance.id)} insurance={insurance}/>
			) 
		}); 
		console.log(insurances)
		return( <table className="table">{thead}<tbody>{insurances}</tbody></table> )
	} 
});
