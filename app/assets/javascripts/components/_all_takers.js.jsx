var AllTakers = React.createClass({ 
	getInitialState() { 
		return {
		 takers: []
		} 
	},
	handleDelete(id) {
        this.props.handleDelete(id);
    },
    onUpdate(taker) {
        this.props.onUpdate(taker);
    },
	componentDidMount() { 
		console.log('Component mounted'); 

	}, 
	render() { 
		var thead = ( <thead  key={"thead"+999999}> 
					    <tr key={"tr"+999999}>
							<th key={"th"+1}>Tipo Doc</th> 
							<th key={"th"+2}>Número</th> 
							<th key={"th"+3}>Nombres</th> 
							<th key={"th"+4}>Apellidos</th> 
							<th key={"th"+5}>Email</th> 
							<th key={"th"+6}>Telefono</th> 
						</tr>
					</thead> )
		var takers = this.props.takers.map((taker) => { 
			return (
				<Taker key={"tk" + taker.id} handleUpdate={this.onUpdate} handleDelete={this.handleDelete.bind(this, taker.id)} taker={taker}/>
			) 
		}); 
		return( <table className="table">{thead}<tbody>{takers}</tbody></table> )
	} 
});
