var _timeout = null;
var NewTaker = React.createClass({ 
	getInitialState() {
		return {taker: {}}
	},
	handleClick() { 
		console.log('handleClick')
		var that = this
		$.ajax({ 	url: '/api/v1/takers', 
					type: 'POST', 
					data: { 
						taker: that.state.taker
					}, success: (taker) => { 
						this.props.handleSubmit(taker);
					} 
				});
	},
	searchTaker(d) {
		var that = this
		clearTimeout(_timeout)
		_timeout = setTimeout(function(){
			console.log('calling_taker')
			$.getJSON('/api/v1/takers/get_taker/',{ docnumber: d }, (response) => { that.setState({taker: response}) });
		},500);
	},
	render() {
		console.log(this.state.taker)
	 return ( 
			<div className="form-group"> 
				<input className="form-control" type="text" onChange={ (e) => this.searchTaker(e.target.value) } ref='docnumber' placeholder='Entre número de documento' /> 
				<input value={this.state.taker.doctype} className="form-control" type="text"   placeholder='Tipo de documento' disabled /> 
				<input value={this.state.taker.names} className="form-control" onChange={ (e) => this.setState({ names: e.target.value }) } ref='names' placeholder='Nombres' /> 
				<input value={this.state.taker.lastnames} className="form-control" onChange={ (e) => this.setState({ lastnames: e.target.value }) } ref='lastnames' placeholder='Apellidos' />
				<input value={this.state.taker.email} className="form-control" type="email" onChange={ (e) => this.setState({ email: e.target.value }) } ref='email' placeholder='Email' />
				<input value={this.state.taker.phone} className="form-control" type="phone"  ref='phone' placeholder='Telefono' />
				<button className="btn btn-primary"  onClick={this.handleClick}>Guardar</button> 
			</div>
		) 
	} 
});
