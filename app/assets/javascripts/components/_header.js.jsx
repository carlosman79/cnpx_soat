var Header = React.createClass({ 
	render() { 
		return ( 
			<nav className="navbar navbar-default navbar-toggleable-md navbar-light bg-faded"> 
				<div className="contanier-fluid" id="navbarSupportedContent">
				 	<div className="navbar-header">
				      <button type="button" className="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
				        <span className="sr-only">Toggle navigation</span>
				        <span className="icon-bar"></span>
				        <span className="icon-bar"></span>
				        <span className="icon-bar"></span>
				      </button>
				      <a className="navbar-brand" href="#">SOAT Kiosk</a>
					</div>
				</div>
				<div className="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
					<ul className="nav navbar-nav">
						 <li onClick={this.menuSelect.bind(this,1)} className="active"><a href="#">Comprar <span className="sr-only">(current)</span></a></li>
						 <li onClick={this.menuSelect.bind(this,2)} ><a href="#">Consultar</a></li>
					</ul>
				</div>
			</nav> 
		) 
	},
	menuSelect (li) {
		this.props.handleMenu(li)
	}
});

