var Main = React.createClass({ 
	getInitialState() {
		return {option: 1}
	},
	handleMenu (id) {
		this.setState({option: id})
		console.log(id)
	},
	render() { 
		return ( <div>  <Header handleMenu={this.handleMenu} /> <Body option={this.state.option} /> </div> ) 
	} 
});
