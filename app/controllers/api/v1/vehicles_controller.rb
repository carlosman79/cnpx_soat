class Api::V1::VehiclesController < Api::V1::BaseController

  before_action :set_vehicle, only: [:show,:update]

  def index
    respond_with Vehicle.all
  end
  def get_types
    respond_with Type.all
  end
  def get_subtypes
    respond_with Type.find(params[:id]).subtypes.all
  end
  def show
    respont_with set_vehicle
  end

  def create
    respond_with :api, :v1, Vehicle.create(vehicle_params)
  end

  def destroy
    respond_with Taker.destroy(params[:id])
  end

  def update
    taker.update_attributes(vehicle_params)
    respond_with item, json: vehicle
  end

  private

  def vehicle_params
    params.require(:taker).permit( :id,
      :doctype,
      :docnumber,
      :names,
      :lastnames,
      :email,
      :phone)
  end
  def set_vehicle
    vehicle = Vehicle.find(params["id"])
  end

end