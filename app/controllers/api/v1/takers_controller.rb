class Api::V1::TakersController < Api::V1::BaseController

  before_action :set_taker, only: [:show,:update]
  before_action :set_taker, only: [:get_taker]
  def index
    respond_with Taker.all
  end

  def show
    respond_with @taker
  end
  def get_taker
    respond_with @taker
  end
  
  def create

    begin
       @taker = Taker.find_by(docnumber: params["docnumber"])
     if @taker.present?
        respond_with :api, :v1, @taker
      else
        respond_with :api, :v1, Taker.create(taker_params)
      end

      
    rescue => ex
      respond_with Taker.find_by(docnumber: taker_params[:docnumber])
    end
    #respond_with @taker
  end

  def destroy
    respond_with Taker.destroy(params[:id])
  end

  def update
    taker.update_attributes(taker_params)
    respond_with item, json: taker
  end

  private

  def taker_params
    params.require(:taker).permit( 
      :doctype,
      :docnumber,
      :names,
      :lastnames,
      :email,
      :phone)
  end
  def set_taker
    if params.has_key?(:id)
      @taker = Taker.find(params["id"])
    end
     if params.has_key?(:docnumber)
      @taker = Taker.find_by(docnumber: params["docnumber"])
    end   
  end

end