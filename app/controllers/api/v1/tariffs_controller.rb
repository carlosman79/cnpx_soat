class Api::V1::TariffsController < Api::V1::BaseController

  before_action :set_tariff, only: [:show,:update]

  def index
    respond_with Tariff.all
  end

  def show
    respont_with set_tariff
  end
  def get_tariff
    # begin
         type_id = get_tariff_params[:type_id].to_i
         attr_type = Tariff::SUBTYPE1[type_id]
         attr_value = nil
        if get_tariff_params.has_key?(attr_type)
          attr_value = get_tariff_params[attr_type].to_i 
          respond_with Type.find(type_id).subtypes.find_by([' ? >= "subtypes".a_value  and  ? <= "subtypes".b_value ',attr_value,attr_value ]).tariffs.first
        end
     # rescue => ex
      #    p ex
       #   respond_with ex
      #end
  end
  def create
    respond_with :api, :v1, Tariff.create(tariff_params)
  end

  def destroy
    respond_with Taker.destroy(params[:id])
  end

  def update
    tariff.update_attributes(tariff_params)
    respond_with item, json: tariff
  end

  private
  def get_tariff_params
      params[:tariff_params].permit(:cils,:weight,:passengers,:age,:type_id)
  end
  def tariff_params
    params.require(:tariff).permit( 
        :a_value,
        :b_value,
        :age,
        :rate,
        :cost,
        :fosyga,
        :subtotal,
        :runt,
        :total,
        :type_id,
        :subtype_id
      )
  end
  def set_tariff
    tariff = Tariff.find(params["id"])
  end

end