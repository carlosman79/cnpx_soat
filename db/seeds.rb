# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

Tariff.delete_all
Taker.delete_all
Vehicle.delete_all
Type.delete_all
Insurance.delete_all
Subtype.delete_all
type1 = Type.create(name: 'MOTOS')
Subtype.create(name: 'Menos de 100cc', a_value: 0, b_value: 100, age: -1, type_id: type1.id)
Subtype.create(name: 'De 100cc a 200cc', a_value: 100, b_value: 200, age: -1, type_id: type1.id)
Subtype.create(name: 'Más de 200cc', a_value: 200, b_value: 9999, age: -1, type_id: type1.id)
Subtype.create(name: 'MOTOCARROS', a_value: -1, b_value: -1, age: -1, type_id: type1.id)

type2 = Type.create(name: 'CAMPEROS Y CAMIONETAS')
Subtype.create(name: 'Menos de 1500cc', a_value: 0, b_value: 1500, age: 9, type_id: type2.id)
Subtype.create(name: 'Menos de 1500cc', a_value: 0, b_value: 1500, age: 10, type_id: type2.id)
Subtype.create(name: '1500cc a 2500cc', a_value: 1500, b_value: 2500, age: 9, type_id: type2.id)
Subtype.create(name: '1500cc a 2500cc', a_value: 1500, b_value: 2500, age: 10, type_id: type2.id)
Subtype.create(name: 'Más de 2500cc', a_value: 2500, b_value: 9999, age: 9, type_id: type2.id)
Subtype.create(name: 'Más de 2500cc', a_value: 2500, b_value: 9999, age: 10, type_id: type2.id)


type3 = Type.create(name: 'CARGA O MIXTO')
Subtype.create(name: 'Menos de 5 toneladas', a_value: 0, b_value: 5, age: -1, type_id: type3.id)
Subtype.create(name: 'De 5 a 15 toneladas', a_value: 5, b_value: 15, age: -1, type_id: type3.id)
Subtype.create(name: 'Más de 15 toneladas', a_value: 15, b_value: 9999, age: -1, type_id: type3.id)

type4 = Type.create(name: 'OFICIALES ESPECIALES')
Subtype.create(name:  'Menos de 1500cc', a_value: 0, b_value: 1500, age: -1, type_id: type4.id)
Subtype.create(name: '1500cc a 2500cc', a_value: 1500, b_value: 2500, age: -1, type_id: type4.id)
Subtype.create(name: 'Más de 2500cc', a_value: 2500, b_value: 9999, age: -1, type_id: type4.id)

type5 = Type.create(name: 'AUTOS FAMILIARES')
Subtype.create(name: 'Menos de 1500cc', a_value: 0, b_value: 1500, age: 9, type_id: type5.id)
Subtype.create(name: 'Menos de 1500cc', a_value: 0, b_value: 1500, age: 10, type_id: type5.id)
Subtype.create(name: '1500cc a 2500cc', a_value: 1500, b_value: 2500, age: 9, type_id: type5.id)
Subtype.create(name: '1500cc a 2500cc', a_value: 1500, b_value: 2500, age: 10, type_id: type5.id)
Subtype.create(name: 'Más de 2500cc', a_value: 2500, b_value: 9999, age: 9, type_id: type5.id)
Subtype.create(name: 'Más de 2500cc', a_value: 2500, b_value: 9999, age: 10, type_id: type5.id)


type6 = Type.create(name: 'VEHICULOS PARA SEIS O MAS PASAJEROS')
Subtype.create(name: 'Menos de 2500cc', a_value: 0, b_value: 2500, age: 9, type_id: type6.id)
Subtype.create(name: 'Menos de 2500cc', a_value: 0, b_value: 2500, age: 10, type_id: type6.id)
Subtype.create(name: 'Más de 2500cc', a_value: 2500, b_value: 9999, age: 9, type_id: type6.id)
Subtype.create(name: 'Más de 2500cc', a_value: 2500, b_value: 9999, age: 10, type_id: type6.id)

type7 = Type.create(name: 'AUTOS DE NEGOCIOS Y TAXIS')
Subtype.create(name: 'Menos de 1500cc', a_value: 0, b_value: 1500, age: 9, type_id: type7.id)
Subtype.create(name: 'Menos de 1500cc', a_value: 0, b_value: 1500, age: 10, type_id: type7.id)
Subtype.create(name: '1500cc a 2500cc', a_value: 1500, b_value: 2500, age: 9, type_id: type7.id)
Subtype.create(name: '1500cc a 2500cc', a_value: 1500, b_value: 2500, age: 10, type_id: type7.id)
Subtype.create(name: 'Más de 2500cc', a_value: 2500, b_value: 9999, age: 9, type_id: type7.id)
Subtype.create(name: 'Más de 2500cc', a_value: 2500, b_value: 9999, age: 10, type_id: type7.id)


type8 = Type.create(name: 'BUSES Y BUSETAS DE SERVICIO URBANO')
Subtype.create(name: '', a_value: -1, b_value: -1, age: -1, type_id: type8.id)

type9 = Type.create(name: 'SERVICIO PUBLICO MUNICIPAL')
Subtype.create(name: 'Menos de 2500cc', a_value: 0, b_value: 2500, age: -1, type_id: type9.id)
Subtype.create(name: 'Menos de 2500cc', a_value: 0, b_value: 2500, age:-1, type_id: type9.id)



moto1 = 
Tariff.create(rate: 8.26, cost: 203100, fosyga: 101550, subtotal: 304650, runt: 1610, total: 306260, subtype_id: 0, death: 800, medics: 750, inability: 180, move: 10)
Tariff.create(rate: 11.09, cost: 272700, fosyga: 136350, subtotal: 409050, runt: 1610, total: 410660 , subtype_id: 1, death: 800, medics: 750, inability: 180, move: 10)
Tariff.create(rate: 12.51, cost: 307600, fosyga: 153800, subtotal: 461400, runt: 1610, total: 463010, subtype_id: 2, death: 800, medics: 750, inability: 180, move: 10)
Tariff.create(rate: 12.51, cost: 307600, fosyga: 153800, subtotal: 461400, runt: 1610, total: 463010, subtype_id: 3, death: 800, medics: 750, inability: 180, move: 10)

Tariff.create(rate: 13.29, cost: 326800, fosyga: 163400, subtotal: 490200, runt: 1610, total: 491810, subtype_id: 4, death: 800, medics: 750, inability: 180, move: 10)

#desde aqui
Tariff.create(rate: 13.29, cost: 326800, fosyga: 163400, subtotal: 490200, runt: 1610, total: 491810, subtype_id: 5, death: 800, medics: 750, inability: 180, move: 10)
Tariff.create(rate: 13.29, cost: 326800, fosyga: 163400, subtotal: 490200, runt: 1610, total: 491810, subtype_id: 6, death: 800, medics: 750, inability: 180, move: 10)
Tariff.create(rate: 13.29, cost: 326800, fosyga: 163400, subtotal: 490200, runt: 1610, total: 491810, subtype_id: 7, death: 800, medics: 750, inability: 180, move: 10)
Tariff.create(rate: 13.29, cost: 326800, fosyga: 163400, subtotal: 490200, runt: 1610, total: 491810, subtype_id: 8, death: 800, medics: 750, inability: 180, move: 10)
Tariff.create(rate: 13.29, cost: 326800, fosyga: 163400, subtotal: 490200, runt: 1610, total: 491810, subtype_id: 9, death: 800, medics: 750, inability: 180, move: 10)
Tariff.create(rate: 13.29, cost: 326800, fosyga: 163400, subtotal: 490200, runt: 1610, total: 491810, subtype_id: 10, death: 800, medics: 750, inability: 180, move: 10)
Tariff.create(rate: 13.29, cost: 326800, fosyga: 163400, subtotal: 490200, runt: 1610, total: 491810, subtype_id: 11, death: 800, medics: 750, inability: 180, move: 10)
Tariff.create(rate: 13.29, cost: 326800, fosyga: 163400, subtotal: 490200, runt: 1610, total: 491810, subtype_id: 12, death: 800, medics: 750, inability: 180, move: 10)
Tariff.create(rate: 13.29, cost: 326800, fosyga: 163400, subtotal: 490200, runt: 1610, total: 491810, subtype_id: 13, death: 800, medics: 750, inability: 180, move: 10)
Tariff.create(rate: 13.29, cost: 326800, fosyga: 163400, subtotal: 490200, runt: 1610, total: 491810, subtype_id: 14, death: 800, medics: 750, inability: 180, move: 10)
Tariff.create(rate: 13.29, cost: 326800, fosyga: 163400, subtotal: 490200, runt: 1610, total: 491810, subtype_id: 15, death: 800, medics: 750, inability: 180, move: 10)
Tariff.create(rate: 13.29, cost: 326800, fosyga: 163400, subtotal: 490200, runt: 1610, total: 491810, subtype_id: 16, death: 800, medics: 750, inability: 180, move: 10)

Tariff.create(rate: 13.29, cost: 326800, fosyga: 163400, subtotal: 490200, runt: 1610, total: 491810, subtype_id: 17, death: 800, medics: 750, inability: 180, move: 10)
Tariff.create(rate: 13.29, cost: 326800, fosyga: 163400, subtotal: 490200, runt: 1610, total: 491810, subtype_id: 18, death: 800, medics: 750, inability: 180, move: 10)
Tariff.create(rate: 13.29, cost: 326800, fosyga: 163400, subtotal: 490200, runt: 1610, total: 491810, subtype_id: 19, death: 800, medics: 750, inability: 180, move: 10)
Tariff.create(rate: 13.29, cost: 326800, fosyga: 163400, subtotal: 490200, runt: 1610, total: 491810, subtype_id: 20, death: 800, medics: 750, inability: 180, move: 10)
Tariff.create(rate: 13.29, cost: 326800, fosyga: 163400, subtotal: 490200, runt: 1610, total: 491810, subtype_id: 21, death: 800, medics: 750, inability: 180, move: 10)
Tariff.create(rate: 13.29, cost: 326800, fosyga: 163400, subtotal: 490200, runt: 1610, total: 491810, subtype_id: 22, death: 800, medics: 750, inability: 180, move: 10)
Tariff.create(rate: 13.29, cost: 326800, fosyga: 163400, subtotal: 490200, runt: 1610, total: 491810, subtype_id: 23, death: 800, medics: 750, inability: 180, move: 10)
Tariff.create(rate: 13.29, cost: 326800, fosyga: 163400, subtotal: 490200, runt: 1610, total: 491810, subtype_id: 24, death: 800, medics: 750, inability: 180, move: 10)
Tariff.create(rate: 13.29, cost: 326800, fosyga: 163400, subtotal: 490200, runt: 1610, total: 491810, subtype_id: 25, death: 800, medics: 750, inability: 180, move: 10)
Tariff.create(rate: 13.29, cost: 326800, fosyga: 163400, subtotal: 490200, runt: 1610, total: 491810, subtype_id: 26, death: 800, medics: 750, inability: 180, move: 10)

Tariff.create(rate: 13.29, cost: 326800, fosyga: 163400, subtotal: 490200, runt: 1610, total: 491810, subtype_id: 27, death: 800, medics: 750, inability: 180, move: 10)
Tariff.create(rate: 13.29, cost: 326800, fosyga: 163400, subtotal: 490200, runt: 1610, total: 491810, subtype_id: 28, death: 800, medics: 750, inability: 180, move: 10)
Tariff.create(rate: 13.29, cost: 326800, fosyga: 163400, subtotal: 490200, runt: 1610, total: 491810, subtype_id: 29, death: 800, medics: 750, inability: 180, move: 10)
Tariff.create(rate: 13.29, cost: 326800, fosyga: 163400, subtotal: 490200, runt: 1610, total: 491810, subtype_id: 30, death: 800, medics: 750, inability: 180, move: 10)
Tariff.create(rate: 13.29, cost: 326800, fosyga: 163400, subtotal: 490200, runt: 1610, total: 491810, subtype_id: 31, death: 800, medics: 750, inability: 180, move: 10)
Tariff.create(rate: 13.29, cost: 326800, fosyga: 163400, subtotal: 490200, runt: 1610, total: 491810, subtype_id: 32, death: 800, medics: 750, inability: 180, move: 10)
Tariff.create(rate: 13.29, cost: 326800, fosyga: 163400, subtotal: 490200, runt: 1610, total: 491810, subtype_id: 33, death: 800, medics: 750, inability: 180, move: 10)
Tariff.create(rate: 13.29, cost: 326800, fosyga: 163400, subtotal: 490200, runt: 1610, total: 491810, subtype_id: 34, death: 800, medics: 750, inability: 180, move: 10)
Tariff.create(rate: 13.29, cost: 326800, fosyga: 163400, subtotal: 490200, runt: 1610, total: 491810, subtype_id: 35, death: 800, medics: 750, inability: 180, move: 10)
Tariff.create(rate: 13.29, cost: 326800, fosyga: 163400, subtotal: 490200, runt: 1610, total: 491810, subtype_id: 36, death: 800, medics: 750, inability: 180, move: 10)
Tariff.create(rate: 13.29, cost: 326800, fosyga: 163400, subtotal: 490200, runt: 1610, total: 491810, subtype_id: 37, death: 800, medics: 750, inability: 180, move: 10)
Tariff.create(rate: 13.29, cost: 326800, fosyga: 163400, subtotal: 490200, runt: 1610, total: 491810, subtype_id: 38, death: 800, medics: 750, inability: 180, move: 10)
Tariff.create(rate: 13.29, cost: 326800, fosyga: 163400, subtotal: 490200, runt: 1610, total: 491810, subtype_id: 39, death: 800, medics: 750, inability: 180, move: 10)
Tariff.create(rate: 13.29, cost: 326800, fosyga: 163400, subtotal: 490200, runt: 1610, total: 491810, subtype_id: 40, death: 800, medics: 750, inability: 180, move: 10)
#hasta qui

taker1 = Taker.create(doctype: 'CC', docnumber: '88252671', names: 'Carlos Manuel', lastnames: 'Patiño Machado', email: 'patmac_p@hotmail.com', phone: '3013512616')
Taker.create(doctype: 'CC', docnumber: '12440101112', names: 'Gabriela', lastnames: 'Perez Martinez', email: 'gabi29199@hotmail.com', phone: '3182712689')


vehicle1 = Vehicle.create(plate: 'ABC123',type_id: type1.id,  age: 1,  passengers: 1, cilinders: 100, tons: 0)

Insurance.create(
       taker_id: taker1.id,
       vehicle_id: vehicle1.id,
       bought_at: DateTime.now,
       until: DateTime.now + 1.year,
       cost: moto1.cost,
       fosyga: moto1.fosyga,
       runt: moto1.runt,
       death: 0,
       medics: 0,
       inability: 0
       )
