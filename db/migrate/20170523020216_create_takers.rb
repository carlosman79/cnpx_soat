class CreateTakers < ActiveRecord::Migration[5.1]
  def change
    create_table :takers do |t|
      t.string :doctype
      t.string :docnumber
      t.string :names
      t.string :lastnames
      t.string :email
      t.string :phone

      t.timestamps
    end
  end
end
