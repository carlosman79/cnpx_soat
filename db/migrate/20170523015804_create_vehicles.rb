class CreateVehicles < ActiveRecord::Migration[5.1]
  def change
    create_table :vehicles do |t|
      t.string  :plate
      t.integer :type_id
      t.integer :subtype_id
      t.integer :age
      t.integer :passengers
      t.integer :cilinders
      t.integer :tons
      t.timestamps
    end
  end
end
