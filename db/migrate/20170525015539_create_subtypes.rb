class CreateSubtypes < ActiveRecord::Migration[5.1]
  def change
    create_table :subtypes do |t|
      t.string :name
      t.integer :a_value
      t.integer :b_value
      t.integer :type_id
      t.integer :age
      t.timestamps
    end
  end
end
