class CreateTariffs < ActiveRecord::Migration[5.1]
  def change
    create_table :tariffs do |t|
      t.float :rate
      t.float :cost
      t.float :fosyga
      t.float :subtotal
      t.float :runt
      t.float :total
      t.integer :type_id
      t.integer :subtype_id
      t.integer :death
      t.integer :medics
      t.integer :inability
      t.integer :move
      t.timestamps
    end
  end
end
