class CreateInsurances < ActiveRecord::Migration[5.1]
  def change
    create_table :insurances do |t|
      t.integer :taker_id
      t.integer :vehicle_id
      t.datetime :bought_at
      t.datetime :until
      t.float :cost
      t.float :fosyga
      t.float :subtotal
      t.float :total
      t.float :runt
      t.float :death
      t.float :medics
      t.float :inability
      t.float :move
      t.timestamps
    end
  end
end
