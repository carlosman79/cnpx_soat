# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170525015539) do

  create_table "insurances", force: :cascade do |t|
    t.integer "taker_id"
    t.integer "vehicle_id"
    t.datetime "bought_at"
    t.datetime "until"
    t.float "cost"
    t.float "fosyga"
    t.float "subtotal"
    t.float "total"
    t.float "runt"
    t.float "death"
    t.float "medics"
    t.float "inability"
    t.float "move"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "subtypes", force: :cascade do |t|
    t.string "name"
    t.integer "a_value"
    t.integer "b_value"
    t.integer "type_id"
    t.integer "age"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "takers", force: :cascade do |t|
    t.string "doctype"
    t.string "docnumber"
    t.string "names"
    t.string "lastnames"
    t.string "email"
    t.string "phone"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "tariffs", force: :cascade do |t|
    t.float "rate"
    t.float "cost"
    t.float "fosyga"
    t.float "subtotal"
    t.float "runt"
    t.float "total"
    t.integer "type_id"
    t.integer "subtype_id"
    t.integer "death"
    t.integer "medics"
    t.integer "inability"
    t.integer "move"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "types", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "vehicles", force: :cascade do |t|
    t.string "plate"
    t.integer "type_id"
    t.integer "subtype_id"
    t.integer "age"
    t.integer "passengers"
    t.integer "cilinders"
    t.integer "tons"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

end
