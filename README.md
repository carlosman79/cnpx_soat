# README

Instalación

    * Clonar el repositorio
    * Entrar al directorio a traveź de la consola 'cnpx_soat'
    * Asegurarse de tener Ruby 2.4.*, luego ejecutar 'bundle install'
    * Ejecutar 'rake db:migrate' seguido de 'rake:seed'
    * Ejecutar el servidor rails 'rails s'
    * Entrar al navegador con 'http://localhost:3000'
    * Probar con placa 'ABC123'
    * Probar con cédula '88252671'

Características de SOAT Kiosk

*    Migración en Rails. Usa Sqlite3 por defecto.
*    Vista usando ReactJS
*    Estilos Css usando Bootstrap


Limitaciones de la versión

*   El Botón de menú 'Consultar' lista todos los SOATs.
*   Permite buscar una placa específica; si esta registrada se lista los seguros activos
*   En ambos casos permite clickar 'Asegurar'
*   Permite elegir Clase de Vehiculo.
*   De acuerdo a la clase se predefinió en un Array los atributos cilindros, pasajeros, peso, edad 
    de acuerdo a la tabla de tarifas. Para dinámicamente buscar la tarifa.
*   En seed.rb se establecieron algunas tarifas otras repetidas con tal de mantener la integridad de las tablas
*   Una vez establecido los 'atributos' se puede clickar en 'Asegurar' nuevamente y despliega la Orden de compra.
*   Al clickar 'Asegurar' una vez más procede a capturar los datos del 'tomador': (usar 88252671), debido a que hay un bug en la integración de react-rails y no deja editar los campos (porque use value en vez de defaultValue;       defaultValue no muestra la información en el compoenente por alguna razón). 

*   Al clickar 'guardar' registra un nuevo SOAT .


Faltantes

*   Validar si el seguro esta activo y setear la fecha 'until' 1 año por encima de la fecha until del seguro anterior.
*   Luego de clicar 'Guardar' en la captura de los datos del tomador; falta la captura de datos de pago
*   Link de exportación a PDF
*   Separar la opción 'Consultar' solo para el Administrador.
*   Unit testing.