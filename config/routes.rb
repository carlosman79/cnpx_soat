Rails.application.routes.draw do
	root to: 'site#index'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
    namespace :api do
      namespace :v1 do
        get 'takers/get_taker', to: 'takers#get_taker'
        resources :takers

        get 'vehicles/get_types', to: 'vehicles#get_types'
        get 'vehicles/get_subtypes', to: 'vehicles#get_subtypes'
        resources :vehicles, only: [:index, :create, :destroy, :update]

        get 'insurances/get_insurances', to: 'insurances#get_insurances'
        resources :insurances


         get 'tariffs/get_tariff', to: 'tariffs#get_tariff'
         resources :tariffs
      end
    end
end